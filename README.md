law_string
==========

String operations to help with online legal publishing.

Functions
---------

* `add_typography()` change simple ASCII quotes to proper quotation marks.