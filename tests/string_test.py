from publiclaw.string import add_typography


def test_leaves_plain_text_untouched():
    simple_text = "Hello, world"
    assert add_typography(simple_text) == simple_text


def test_changes_a_single_quote():
    quote = "A leopard's spots"
    assert add_typography(quote) == "A leopard’s spots"
